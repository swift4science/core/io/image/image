import argparse
from collections import namedtuple
from imutils.paths import list_images
import subprocess
import json
import sys

image_meta = namedtuple(
    "image_meta", ["filename", "width", "height", "bits_per_sample"])


def parse_imagemagick_row(row):
    row_elements = row.split(": ")
    key = row_elements[0]
    value = None
    if len(row_elements) > 1:
        value = row_elements[1].strip()
    indentation = len(key) - len(key.lstrip(" \t"))
    key = key.strip().lower().replace(" ", "_")
    key = key.rstrip(":")
    return indentation, key, value


def last_row(output_array, current_index):
    return current_index == (len(output_array) - 1)


def should_add_subrows(row_indentation, indentation, imagemagick_output, sub_rows, current_row):
    return (row_indentation == indentation or last_row(imagemagick_output, current_row)) and len(sub_rows) > 0


def parse_geometry(value):
    geometry = value.replace('x', '+').split('+')
    return {
        "width": int(geometry[0]),
        "height": int(geometry[1]),
        "x-offset": int(geometry[2]),
        "y-offset": int(geometry[3])
    }


def parse_value(key, value):
    if key == "geometry" or key == "page_geometry":
        return parse_geometry(value)

    try:
        value = int(value)
        return value
    except ValueError:
        pass

    try:
        value = float(value)
        return value
    except ValueError:
        pass

    if value.lower() == "true":
        return True
    elif value.lower() == "false":
        return False
    return value


def imagemagick_to_json(imagemagick_output, indentation=0):
    imagemagick_dict = dict()
    last_key = ""
    subrow_indentation = sys.maxsize
    sub_rows = []
    for i, row in enumerate(imagemagick_output):
        row_indentation, key, value = parse_imagemagick_row(row)
        if row_indentation > indentation:
            if row_indentation < subrow_indentation:
                subrow_indentation = row_indentation
            sub_rows.append(row)

        if should_add_subrows(row_indentation, indentation, imagemagick_output, sub_rows, i):
            imagemagick_dict[last_key] = imagemagick_to_json(
                sub_rows, subrow_indentation)
            sub_rows = []

        if row_indentation == indentation:
            if value is not None:
                imagemagick_dict[key] = parse_value(key, value)
            last_key = key
    return imagemagick_dict


def get_image_metadata(image_filename):
    process = subprocess.Popen(
        ['identify', '-verbose', image_filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    image_meta = [row for row in out.decode(
        'ascii').split("\n") if len(row) > 0]
    # resolution = image_meta_array[2]
    # filename = image_meta_array[0].replace(
    #    "../", "").replace("[0]", "").replace("[1]", "")
    # width, height = resolution.split('x')
    # bits_per_sample = image_meta_array[4].replace('-bit', '')
    return imagemagick_to_json(image_meta)


def generate_code(image_metadata):
    return f'   .init(filename: "{image_metadata.filename}", width: {image_metadata.width}, height: {image_metadata.height}, bitsPerSample: {image_metadata.bits_per_sample}),\n'


parser = argparse.ArgumentParser()
parser.add_argument("-d", "--image-directory", required=True,
                    help="The directory containing the images")
parser.add_argument("-n", "--number-of-images", default=None,
                    type=int, help="The number of images to use")
parser.add_argument("-o", "--output-file", required=True,
                    help="The swift file containing the generated metadata")
parser.add_argument("-e", "--extension", required=True,
                    help="The file extension for the files to select")
args = parser.parse_args()

images = [image_filename for image_filename in list_images(
    args.image_directory) if image_filename.endswith(args.extension)]

number_of_images = args.number_of_images
if not number_of_images:
    number_of_images = len(images)

selected_images = images[:number_of_images]

with open(args.output_file, 'w') as output_file:
    image_meta_array = [get_image_metadata(
        image_filename) for i, image_filename in enumerate(selected_images) if i < args.number_of_images]
    json.dump(image_meta_array, output_file, indent=4)
