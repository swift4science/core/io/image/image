// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "image",
    platforms: [
        .macOS(.v12)
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Image",
            targets: ["Image"]),
    ],    
    dependencies: [
        .package(url: "https://github.com/JohnSundell/Files", from: "4.0.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .systemLibrary(
            name: "Clibtiff",
            path: "./Libraries/Clibtiff",
            pkgConfig: "libtiff-4",
            providers: [
                .brew(["libtiff"]),
                .apt(["libtiff-dev"]),
                ]
        ),
        .systemLibrary(
            name: "Clibpng",
            path: "./Libraries/Clibpng",  
            pkgConfig: "libpng",
            providers: [
                .brew(["libpng"]),
                .apt(["libpng-dev"])
                ]                  
        ),
        .target(
            name: "Image",
            dependencies: ["Clibtiff", "Clibpng", "Files"],
            path: "Sources"
            ),
        .testTarget(
            name: "ImageTests",
            dependencies: ["Image", "Files"],
            resources: [
                .copy("Resources/")
            ]
        ),
            
    ]
)
