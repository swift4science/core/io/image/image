import Clibtiff

//TODO: Move each image class to its own package alongside its libraries
class Tiff: Image {
    let tiffFilePtr: OpaquePointer?

    init?(filename: String) {
        guard let tiffFilePtr = TIFFOpen(filename, "r") else {
            return nil
        }
        self.tiffFilePtr = tiffFilePtr
    }

    deinit {
        TIFFClose(tiffFilePtr)
    }

    var width: Int {
        return Int(getTiffField(tiffTag: TIFFTAG_IMAGEWIDTH))
    }

    var height: Int {
        return Int(getTiffField(tiffTag: TIFFTAG_IMAGELENGTH))
    }

    var bitsPerSample: Int {
        return Int(getTiffField(tiffTag: TIFFTAG_BITSPERSAMPLE))
    }

    var colorSpace: ColorSpace {
        //getTiffField(tiffTag: EXIFTAG_COLORSPACE)
        return .sRGB
    }

    func save(filename: String) {
        
    }
    
    private func getTiffField(tiffTag: Int32) -> Int32 {
        var fieldValue: Int32 = 0
        withUnsafeMutablePointer(to: &fieldValue) { valuePtr in
            withVaList([valuePtr]) { vaListPtr in
                TIFFVGetField(tiffFilePtr, UInt32(tiffTag), vaListPtr)
            }
        }
        return fieldValue
    }
    
}
