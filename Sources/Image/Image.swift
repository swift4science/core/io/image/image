protocol Image {
    var width: Int { get }
    var height: Int { get }
    var bitsPerSample: Int { get }
    var colorSpace: ColorSpace { get }
    
    func save(filename: String)
}
