//
//  File.swift
//  
//
//  Created by T De Jong on 16/11/2021.
//

import Foundation

class TestUtils
{
    static func pathForTestData(filename: String) -> String
    {
        return Bundle.module.resourceURL?.appendingPathComponent(filename).absoluteString ?? ""
    }
}
