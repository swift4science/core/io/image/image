import XCTest

@testable import Image
import Files

struct ImageInfo {
    var filename: String
    var width: Int
    var height: Int
    var bitsPerSample: Int
}

final class ImageTests: XCTestCase {
    let images: [ImageInfo] = [
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/jim___ah.tif"), width: 664, height: 813, bitsPerSample: 1),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/zackthecat.tif"), width: 234, height: 213, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/quad-lzw.tif"), width: 512, height: 384, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-02.tif"), width: 73, height: 43, bitsPerSample: 2),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/cramps-tile.tif"), width: 800, height: 607, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-12.tif"), width: 73, height: 43, bitsPerSample: 12),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-10.tif"), width: 73, height: 43, bitsPerSample: 10),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-separated-planar-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-24.tif"), width: 73, height: 43, bitsPerSample: 24),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-24.tif"), width: 73, height: 43, bitsPerSample: 24),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-14.tif"), width: 73, height: 43, bitsPerSample: 14),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/jim___gg.tif"), width: 277, height: 339, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/dscf0013.tif"), width: 640, height: 480, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-palette-04.tif"), width: 73, height: 43, bitsPerSample: 4),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-32.tif"), width: 73, height: 43, bitsPerSample: 32),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-12.tif"), width: 73, height: 43, bitsPerSample: 12),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-24.tif"), width: 73, height: 43, bitsPerSample: 24),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/pc260001.tif"), width: 640, height: 480, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-14.tif"), width: 73, height: 43, bitsPerSample: 14),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/text.tif"), width: 1512, height: 359, bitsPerSample: 4),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/quad-tile.tif"), width: 512, height: 384, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-palette-02.tif"), width: 73, height: 43, bitsPerSample: 2),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/quad-jpeg.tif"), width: 512, height: 384, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/ycbcr-cat.tif"), width: 250, height: 325, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-02.tif"), width: 73, height: 43, bitsPerSample: 2),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/cramps.tif"), width: 800, height: 607, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-32.tif"), width: 73, height: 43, bitsPerSample: 32),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/smallliz.tif"), width: 160, height: 160, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-12.tif"), width: 73, height: 43, bitsPerSample: 12),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/off_luv24.tif"), width: 333, height: 225, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/g3test.tif"), width: 1728, height: 1103, bitsPerSample: 1),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-palette-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/off_luv32.tif"), width: 333, height: 225, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-10.tif"), width: 73, height: 43, bitsPerSample: 10),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/strike.tif"), width: 256, height: 200, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/oxford.tif"), width: 601, height: 81, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-04.tif"), width: 73, height: 43, bitsPerSample: 4),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-02.tif"), width: 73, height: 43, bitsPerSample: 2),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-04.tif"), width: 73, height: 43, bitsPerSample: 4),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-separated-contig-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/jim___dg.tif"), width: 277, height: 339, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/jim___cg.tif"), width: 277, height: 339, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-04.tif"), width: 73, height: 43, bitsPerSample: 4),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-contig-32.tif"), width: 73, height: 43, bitsPerSample: 32),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-separated-contig-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/ladoga.tif"), width: 158, height: 118, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/off_l16.tif"), width: 333, height: 225, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-14.tif"), width: 73, height: 43, bitsPerSample: 14),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/jello.tif"), width: 256, height: 192, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-separated-planar-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-16.tif"), width: 73, height: 43, bitsPerSample: 16),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/caspian.tif"), width: 279, height: 220, bitsPerSample: 64),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-minisblack-06.tif"), width: 73, height: 43, bitsPerSample: 6),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/fax2d.tif"), width: 1728, height: 1082, bitsPerSample: 1),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-palette-08.tif"), width: 73, height: 43, bitsPerSample: 8),
        .init(filename: TestUtils.pathForTestData(filename: "Resources/Tiff/flower-rgb-planar-10.tif"), width: 73, height: 43, bitsPerSample: 10),
    ]

    func testWidthReadCorrectly() {
        
        for imageWithInfo in loadImages(images: self.images) {
            XCTAssertEqual(
                imageWithInfo.info.width, imageWithInfo.image.width,
                "Width incorrect for \(imageWithInfo.info.filename)")
        }
    }

    func testHeightReadCorrectly() {
        for imageWithInfo in loadImages(images: self.images) {
            XCTAssertEqual(
                imageWithInfo.info.height, imageWithInfo.image.height,
                "Height incorrect for \(imageWithInfo.info.filename)")
        }
    }

    func testDepthReadCorrectly() {
        for imageWithInfo in loadImages(images: self.images) {
            XCTAssertEqual(
                imageWithInfo.info.bitsPerSample, imageWithInfo.image.bitsPerSample,
                "bitsPerSample incorrect for \(imageWithInfo.info.filename)")
        }
    }

    func loadImages(images: [ImageInfo]) -> [(info: ImageInfo, image: Image)] {
        var imagesWithInfo = [(info: ImageInfo, image: Image)]()
        for imageInfo in images {
            guard let tiffFile = Tiff(filename: imageInfo.filename) else {
                XCTFail("Tiff file \(imageInfo.filename) could not be read")
                return []
            }
            imagesWithInfo.append((info: imageInfo, image: tiffFile))
        }
        return imagesWithInfo
    }

    static var allTests = [
        ("testWidthReadCorrectly", testWidthReadCorrectly),
        ("testHeightReadCorrectly", testHeightReadCorrectly),
        ("testDepthReadCorrectly", testDepthReadCorrectly),
    ]
}
